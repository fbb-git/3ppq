#include "main.ih"

void endecrypt1()
{
    ofstream out = Exception::factory<ofstream>("endecrypt1.cpp");

    out << R"(
#include "../support/endecrypt/endecrypt.ih"

EnDeCrypt::EnDeCrypt()
:
    d_key{
        )";

    size_t count = 0;
    for (unsigned char ch: key)
    {
        out << static_cast<size_t>(ch);

        out.put(++count != key.size() ? ',' : '}');

        if (count % 10 == 0)
            out << "\n"
                   "        ";
    }

    out << "\n"
           "{}\n";
}
    

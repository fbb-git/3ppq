#include "main.ih"

void generateKey()
{
    key.resize(keyLength);

    Random random{ from, to, mtSeed };

    generate(key.begin(), key.end(), 
        [&]()
        {
            return random();
        }
    );
}
    

//#define XERR
#include "main.ih"

size_t keyLength;
size_t from;
size_t to;
size_t mtSeed;

vector<unsigned short> key;

int main()
try
{
    ifstream in = Exception::factory<ifstream>("input");

    if (not (in >> keyLength >> from >> to >> mtSeed))
        throw Exception{} << 
                "'input' format error(s): see README for details";

    generateKey();
    endecrypt1();
}
catch (exception const &exc)
{
    cerr << exc.what() << '\n';
}

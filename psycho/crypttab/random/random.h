#ifndef INCLUDED_RANDOM_
#define INCLUDED_RANDOM_

#include <random>

class Random
{
    std::mt19937 d_engine;
    std::uniform_int_distribution<size_t> d_dist;

    public:
        Random(size_t minimum, size_t maximum, size_t mtSeed);

        size_t operator()();            // generates the next random value
};

inline size_t Random::operator()()
{
    return d_dist(d_engine);
}
        
#endif

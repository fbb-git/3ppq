#include "client.ih"

void Client::accept(string const &query)
{
    lg() << "Client accept: " << query << endl;


    WIPdata wipData{ query };
    LockGuard guard{ wipData.lg() };

    wipData.read();                     // if reading fails, throws an
                                        // error-enum value

    string hash = query.substr(query.find_first_of('_') + 1);
    if (hash.length() != Tools::HASH_TXT_SIZE)
    {
        lg() << "incorrect hash length" << endl;
        throw Tools::NO_QUERY;
    }

lg() << "query hash: `" << hash << "', client login: " << 
wipData.clientLogin() << "      hash: `" << loginHash(wipData.clientLogin()) <<
'\'' << endl;

    if (hash == loginHash(wipData.clientLogin()))   // self/meta ratings use
    {
        clientPage(wipData);                        // identical hashes
        return;
    }

    lg() << "hash = " << hash << endl;
                                                        // otherwise activate
    for (size_t idx = 0; idx != Tools::N_OTHER; ++idx)  // an 'other' rater
    {
        lg() << "Idx: " << idx << ", otherlogin: " << 
                    wipData.otherLogin(idx) << ", hash: " << 
                    loginHash(wipData.otherLogin(idx)) << endl;

        if (hash == loginHash(wipData.otherLogin(idx)))
        {
            lg() << "login-hash matches hashed other login code" << endl;

            otherPage(wipData, idx);
            return;
        }
    }

    throw Tools::NO_QUERY;
}





#include "client.ih"

void Client::checkCompleted(WIPdata &wipData) const
{
    if (
        wipData.selfRatings().front() == 0    or
        wipData.metaRatings().front() == 0    or
        not otherRatingsCompleted(wipData)
    )
        return;

    lg() << "data collection " << wipData.pidCid() << " completed." << endl;

    writeCSV(wipData);

    size_t pwd = Tools::random(1000, 9999);

    Psych psych{ g_options.dataDir() };
    if (not psych.deactivateClient(pwd, wipData))           // 3.cc
        throw Tools::NO_PSYCH;

    Report report{ wipData, pwd, psych.fullName() };
    report.generate();
}








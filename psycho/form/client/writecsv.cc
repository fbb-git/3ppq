#include "client.ih"

void Client::writeCSV(WIPdata const &wipData) const
{
    lg() << "writing " << g_options.reportsDir() << wipData.pidCid() 
                << ".csv" << endl;

    ofstream csvData;               // write the CSV data into reports/
    Exception::open(csvData, 
                    g_options.reportsDir() + wipData.pidCid() + ".csv");
    csvData << wipData;
}


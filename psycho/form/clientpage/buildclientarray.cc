#include "clientpage.ih"

size_t ClientPage::buildClientArray(string *array)
{
    size_t idLength = 0;

    for (auto const &client: d_clients)
        idLength = ::max(idLength, client.ident().length());

    ++idLength;

    string pid = to_string(d_psychID);
    string reportPre = g_options.reportsDir() + pid + '.';

    ostringstream out;

    size_t idx = 0;
    for (auto const &client: d_clients)     // construct the client array
    {
//    lg() << client.id() << ": active = " << client.active() << 
//                            ", login0 = " << client.login0() << endl;

        if (client.active() and not WIPdata::exists(d_psychID, client.ident()))
        {
            lg() << "deactivating client " << 
                    d_psychID << '.' << client.ident() << 
                    ": no WIPdata" << endl;

            WIPdata::remove(d_psychID, client.ident());
            d_deactivated.push_back(idx);
        }
        ++idx;

        d_reportExists.push_back(client.zipPwd());

        if 
        (
            not client.zipPwd() 
            and
            Tools::rwExists(reportPre + client.ident() + ".zip")
        )
            unlink((reportPre + client.ident() + ".zip").c_str());

//  see also js/clientpage.js
//      0       1       2     3           4           5         6
//      ID    sex  active                                     rapport-pwd
//      [1,     0,      0,    "Name", "lastname",   "email",    xxx/0]

        size_t zipPwd = d_reportExists.back() ? client.zipPwd() : 0;

        out << setw(11) << "[\"" <<   
                client.ident()      << "\","    <<      //  0
                client.gender()     << ','      <<      //  1
                client.active()     << ",\""    <<      //  2
                client.name()       << "\",\""  <<      //  3
                client.lastName()   << "\",\""  <<      //  4
                client.eMail()      << "\","    <<      //  5
                zipPwd              << "],\n";          //  6
    }
    *array = out.str();

    return idLength;
}


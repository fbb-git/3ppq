#include "clientpage.ih"

    // originally called clientpage.js's report(idx) function, but the
    // click-action is not needed anymore as the results are available in
    // a final .zip

string ClientPage::reportHyperlink(size_t idx) const
{
    return idx >= d_clients.size() || not d_reportExists[idx] ? 
            ""s
        :
            "<a href='#' title='open de rapport zip download pagina' "
                "onclick='report(" +
                to_string(d_psychID) + ", " + to_string(idx) + ")'>"
                "rapportage</a>\n";
}

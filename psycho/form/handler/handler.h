#ifndef INCLUDED_HANDLER_
#define INCLUDED_HANDLER_

#include <string>
#include <unordered_map>

#include <bobcat/cgi>

#include "../../support/options/options.h"

#include "../client/client.h"
#include "../psych/psych.h"

class Handler
{
    FBB::CGI            d_cgi;

    Client d_client;
    Psych d_psych;

    public:
        Handler();
        ~Handler();
        void process();

    private:
        void psychoStart();
        void psychoRequest();

        void logParams();
};

#endif



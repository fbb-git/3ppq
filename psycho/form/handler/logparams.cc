#include "handler.ih"

void Handler::logParams()
{
    lg() << "error-exception. received parameters:" << endl;

    for (auto const &param: d_cgi)
    {
        lg() << param.first << ": " << endl;
        for (auto const &value: param.second)
            lg() << "  `" << value << endl;
    }
}

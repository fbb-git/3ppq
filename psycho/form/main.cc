#include "main.ih"

namespace {

#include "../basedir.f"         // abs. path to the website's base directory

ostringstream s_log;            // to contain this process's log messages

    // to see the log messages in apache's error-log file, initialize
    // s_logBuffer with cerr:
LogBuf s_logBuffer{ cerr }; // the logbuffer filling s_log

//LogBuf s_logBuffer{ s_log }; // the logbuffer filling s_log

} // anonymous


Options g_options{ g_base };

            // m_ identifiers are only used by main and global functions

ostream     m_log{ &s_logBuffer };  // the stream to receive the text to log
bool        m_debug = false;
ofstream    m_devNull{ 0 };

size_t      g_logID = 0;                        // user ID or system ID (= 0)

ConfigFile  g_config{ g_options.config(), ConfigFile::RemoveComment };
Mailer      g_mailer;

int main(int argc, char **argv)
try
{
    if (argc > 1 and "--version"s == argv[1])
    {
        cout << "V " << version << '\n';
        return 0;
    }

    prepareLog();                       // support/global

    lg() << "form: main() starts" << endl;

    Handler handler;                    // Form handling object

    handler.process();                  // process incoming forms

//  cerr << __FILE__" " << 4 << '\n';
    flushLogs(s_log);
//  cerr << __FILE__" " << 5 << '\n';
}
catch ([[maybe_unused]] Tools::ForkChild done)
{}                                      // automatic 0 return
catch (exception const &exc)
{
    m_log.clear();
    lg() << "EXCEPTION: " << exc.what() << endl;

    flushLogs(s_log);
}
catch (...)
{
    m_log.clear();
    lg() << "Unexpected exception" << endl;

    flushLogs(s_log);
}


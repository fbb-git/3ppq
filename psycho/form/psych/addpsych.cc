#include "psych.ih"

//    var name =      form["name"].value;
//    var lastName =  form["lastName"].value;
//    var email =     form["email"].value;
//    var pwd =       form["pwd"].value;
//    var pwd2 =      form["pwd2"].value;
//    var field =     form["field"].value;

void Psych::addPsych()
{
    lg() <<  __FILE__ << endl;

    if (not validRegistrationData(&d_nip, &d_field))
    {
        d_display.out("rejectpsych.h");
        return;
    }

    d_eMail     = d_cgi->param1("email");

    string key = Tools::b64hash(d_eMail);       // get the e-mail b64 name 

    if (d_data.exists(key))                     // key already exists
        d_display.out("knownpsych.h");          // show error
    else
    {
        string pwd{ d_cgi->param1("pwd") };
    
        d_time      = time(0);                  // assign remaining fields
        d_ack       = Tools::random(1000, 9999);
        d_nClients  = 0;
        d_pwdHash   = Tools::md5hash(pwd);
        d_name      = d_cgi->param1("name");
        d_lastName  = d_cgi->param1("lastName");
        d_ID        = d_data.nextNr();

        d_data.write(key, toString());          // write new datafile
                                                // (DataStore)

        d_display.out("ackcheck.h");            // show info about add-check
    }
}








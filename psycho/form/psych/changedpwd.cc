#include "psych.ih"

bool Psych::changedPwd()
{
    lg() <<  __FILE__ << endl;

    string newEmail = d_cgi->param1("newemail");

    string newPwd = d_cgi->param1("newpwd");

    if (newPwd.length() > 0)
        d_pwdHash = Tools::md5hash(newPwd);

    string key = Tools::b64hash(emailCGI());    // get the current eMail key

    std::vector<LockGuard> wipLg;               // used when the psych's 
                                                // e-mail was changed.
                                                // (kept until the new psych 
                                                //  address was stored).

    bool mailChanged = newEmail != d_eMail;
    if (mailChanged)                            // e-mail has changed
    {
        string oldKey = key;                    // old filename
        d_eMail = newEmail;                     // store new eMail
        key = Tools::b64hash(d_eMail);          // compute the new filename

        lg() << __FILE__ ": renamed " << oldKey << " to " << key << '\n';

        d_data.rename(oldKey, key);             // rename the data file

                                                // update e-mail addresses 
        wipLg = updateWIPemail();               // in WIP files
    }

    d_data.write(key, toString());

    if (newPwd.length() > 0 or mailChanged)     // new password or mail addr.
    {                                           // then login once again
        d_display.out("psychpage.h");
        return true;
    }

    return false;
}

#include "psych.ih"

void Psych::clientPage()
{
    lg() <<  __FILE__ " request = `" << d_cgi->param1("request") << '\'' << endl;

    ClientPage::Info info = { s_add + s_addActive };

//        "valid client data: active: " <<  d_cgi->param1("active") << 
//           ", form-email: " <<  d_cgi->param1("clEmail") << 
//           ", email: " << d_cgi->param1("email") << 
//           ", ID: " << d_cgi->param1("ident") << 
//           ", gender: " << d_cgi->param1("gender") << 
//           ", name: " << d_cgi->param1("name") << 
//           ", lastname: " << d_cgi->param1("lastName") << endl;

    
    bool updated = false;
    if (not get(emailCGI()))
        throw Tools::NO_PSYCH;

                                // all forms must have the psych's e-mail
    requireEqual("email", d_eMail); 

    info.clientIdx = d_client.size();

    if (                        // determine the function to perform
        auto iter = s_clientPageRequest.find(d_cgi->param1("request")); 
        iter != s_clientPageRequest.end()
    )
    {
        info = (this->*iter->second)();    
                                    // add, addActive, deactivate, show, 
                                    // update, updateActive, remove, 
        updated = true;
    }

    lg() <<  "clientpage client idx = " << info.clientIdx <<  endl;

//  set info.clientIdx to d_client.size() to clear the fields in, e.g.,
//  activate().

    ClientPage page{ d_display, d_client, d_ID };

    lg() << "displaying the page for client " << info.clientIdx << endl;

    if (page.display(info, d_eMail) or updated)
    {
        for (size_t idx: page.deactivated())
            d_client[idx].deactivate();

        lg() << "updating..." << endl;
        put(emailCGI());
    }
}








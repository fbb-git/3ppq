#include "psych.ih"

void Psych::deactivateClient(WIPdata const &wipData, size_t zipPwd)
{
    lg() <<  __FILE__ << endl;

    string eMail = wipData.psychEmail();

    if (not get(eMail))
    {
        lg() <<  "Psych::deactivate2: cannot retrieve data for psych. " <<
                    eMail << endl;
        return;
    }

    string const &clientIdent = wipData.clientIdent();

    for (PsychClient &client: d_client)
    {
        if (client.ident() == clientIdent)
        {
            client.deactivate();
            client.setZipPwd(zipPwd);
            put(eMail);
            return;
        }            
    }
}

#include "psych.ih"

bool Psych::deactivateClient(size_t pwd, WIPdata const &wipData)
{
    lg() <<  __FILE__ << endl;

    string data;

    string key = Tools::b64hash(wipData.psychEmail());

    if (not d_data.read(&data, key))
        return false;

    read(data);
    deactivateClient(wipData, pwd);

    d_data.write(key, toString());
    return true;
}





#include "psych.ih"

bool Psych::get(std::string const &eMail)
{
    lg() <<  __FILE__" eMail: " << eMail  << endl;

    string data;

    string key = Tools::b64hash(eMail);                 // CTR!
    lg() <<  __FILE__" base64 key: " << key  << endl;

    if (not d_data.read(&data, key))
        return false;

    read(data);
    return true;
}





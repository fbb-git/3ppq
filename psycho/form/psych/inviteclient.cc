#include "psych.ih"

void Psych::inviteClient(PsychClient &client)
{
    lg() <<  __FILE__ << endl;

    client.activate();

                                    // wipdata1.cc: create initialized 
                                    // WIP file
    WIPdata wipData{ d_eMail, d_ID, client.ident(), 
                     client.name(),
                     client.lastName(), 
                     client.gender() };

    string psychName{ d_name + ' ' + d_lastName };

    string link =  Tools::link(d_ID, client.ident(), wipData.clientLogin());
    lg() <<  "inviteClient: link = " << link << endl;

    lg() << g_mailer.sendmail(
            d_eMail,
            client.eMail(), 
            "Verzoek namens " + psychName + " om een vragenlijst in te vullen",
            DollarText::replaceStream(
                g_options.mailDir() + "activateclient", 
                {
                    client.genderText(),                        // $0
                    client.lastName(),                          // $1
                    psychName,                                  // $2
                    link,                                       // $3
                }
            )
        ) << endl;
}

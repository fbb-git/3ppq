#include "psych.ih"

// static
string Psych::newPassword()
{
    string ret;

    while (ret.size() != Tools::MIN_PWD_LENGTH)
    {
                                    // avoid a space as pwd char.
        int ch = ' ' + 1 + Tools::random('z' - ' ' );
        if 
        (
            isalnum(ch) 
            or 
            R"R(-.,\/#!$%\^&\*;:{}=`~())R"s.find(ch) != string::npos
        )
            ret += ch;
    }

    return ret;
}

#include "psych.ih"

void Psych::newPwd()
{
    requireOneOf("email", "@");

    string pwd = newPassword();

    if (not get(emailCGI()))
        return;

    d_pwdHash = Tools::md5hash(pwd);
    put();

    lg() << g_mailer.sendmail(
                d_eMail, 
                d_eMail, 
                "Nieuw wachtwoord 3ppq.nl",
                DollarText::replaceStream(
                    g_options.mailDir() + "newpwd",
                    {
                        d_name,
                        d_lastName,
                        pwd
                    }
                )
            ) << endl;

    d_display.out("newpwd.h");
}



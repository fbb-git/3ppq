#include "psych.ih"

//     <!-- field=1&field=2... -->

uint8_t Psych::parseField()
{
    lg() <<  __FILE__ << endl;

    uint8_t ret = 0;
    for (string const &value: d_cgi->param("field"))
    {
        lg() <<  "field = " << value << endl;
        ret |= stoul(value);
    }

    return ret;
}           



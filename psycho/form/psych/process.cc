#include "psych.ih"

void Psych::process()
{
    lg() <<  __FILE__ ": state: " << d_cgi->param1("state") << 
            ", e-mail: " << d_cgi->param1("email") << 
            ", show: " << d_cgi->param1("show") << endl;


    auto iter = s_state.find(d_cgi->param1("state")); 
    if (iter == s_state.end())
        throw Tools::NO_STATE;

    (this->*(iter->second))();      // data.cc s_state defines the functions
                                    // called when a state is encountered. 
                                    // e.g., state 'verify' calls function
                                    // Psych::verify
}














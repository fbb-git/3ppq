#include "psych.ih"

void Psych::put(string const &eMail)
{
    lg() <<  __FILE__ << endl;

    d_data.write(Tools::b64hash(eMail), toString());
}


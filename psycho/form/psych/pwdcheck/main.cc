#include "main.ih"

// Room for Args initialization

int main()
{
    cout << "enter potential passwords:\n";

    while (true)
    {
        cout << "? ";
        string line;
        if (not getline(cin, line))
            break;

        if (pwdRequirements(line))
            cout << "OK\n";
    }
}


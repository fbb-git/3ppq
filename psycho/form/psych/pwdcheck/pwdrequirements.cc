#include "main.ih"

namespace {

    Pattern hasDigit{ "\\d" };
    Pattern hasLetter{ "[[:alpha:]]" };
//    Pattern hasSpecial{ R"R([-.,\\/#!$%^&*;:{}=`~()])R" };

    char const *msg;
}

bool pwdRequirements(string const &pwd)
try
{
    msg = "missing digits";
    hasDigit.match(pwd);

    msg = "missing letters";
    hasLetter.match(pwd);

//    msg = "missing special chars";
//    hasSpecial.match(pwd);

    bool OK = pwd.length() >= Tools::MIN_PWD_LENGTH;

    if (not OK)
        cout << "Pwd length must be at least " << Tools::MIN_PWD_LENGTH <<
                                                                        '\n';
    return OK;
}
catch (exception const &exc)
{
    cout <<  msg << '\n';
    return false;
}
catch (...)
{
    cout << "unexpected exception!\n";
    return false;
}


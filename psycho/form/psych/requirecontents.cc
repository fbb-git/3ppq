#include "psych.ih"

void Psych::requireContents(char const *name)
{
    if (Tools::checkParam(*d_cgi, name))
        return;

    lg() <<  __FILE__ ": cgi param " << name << " `" << d_cgi->param1(name) << 
                                            "': no contents" << endl;
    throw Tools::REQUIRE;
}

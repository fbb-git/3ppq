#include "psych.ih"

long long Psych::requireNumber(char const *name)
try
{
    return stoll(d_cgi->param1(name));
}
catch (...)
{
    lg() <<  __FILE__ ": cgi param " << name << " `" << d_cgi->param1(name) << 
                                                            "': NaN" << endl;

    throw Tools::REQUIRE;
}

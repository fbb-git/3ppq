#include "psych.ih"

string Psych::toString() const
{
    ostringstream out;                      // encrypt confidential data

    lg() << __FILE__" encrypting conf. data" << endl;

    lg() << "   binary nip = " << 
        Tools::writeN(out, &d_nip) << endl;

    lg() << "   binary d_field = " << 
        Tools::writeN(out, &d_field) << endl;

    out <<  d_name      << '\n' <<
            d_lastName  << '\n' <<
            d_eMail     << '\n';

    lg() << "   " << d_name << ' ' << d_lastName << ", " << d_eMail << endl;

    string iv = Tools::iv();

    lg() << "   IV length: " << iv.size() << endl;

    string encrypted = Tools::encrypt(iv, out.str());

    lg() << "   encrypted length = " << encrypted.size() << endl;

    out.str("");                            // convert client info to string
    for (auto const &client: d_client)    
    {
        string clientStr = client.toString();   // all data of this client
        uint16_t size = clientStr.size();       // its size
        Tools::writeN(out, &size);
        Tools::writeS(out, clientStr);
    }
    string clientData = out.str();

    lg() << __FILE__" client data: `" << clientData  << '\'' << endl;

    out.str("");                            // write the Psych's data    

    Tools::writeS(out, iv);                 // first the encryption IV
    Tools::writeN(out, &d_time);
    Tools::writeN(out, &d_ack);
    Tools::writeN(out, &d_nClients);
    Tools::writeN(out, &d_ID);
    Tools::writeS(out, d_pwdHash);

    uint16_t size = encrypted.size();       // write size of encrypted data
    Tools::writeN(out, &size);
    Tools::writeS(out, encrypted);          // and the data themselves    


//lg() <<  "writing data of " << d_client.size() << " clients" << endl;

    size = d_client.size();             // # of client data elements
    Tools::writeN(out, &size);
    if (size)
        Tools::writeS(out, clientData);     // write the client data

    return out.str();                       // return the binary string    
}




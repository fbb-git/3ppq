#include "psych.ih"

    // visit all WIP files of the current psychologist and 
    // change the e-mail addresses of the psychologist.

vector<LockGuard> Psych::updateWIPemail() const
{
    lg() <<  __FILE__ << endl;

    Glob glob{                      // find all files of psych d_ID
            g_options.dataDir() + to_string(d_ID) + ".*", 
            Glob::NOMATCH, Glob::DEFAULT 
        };

    vector<LockGuard> guard;

    for (string name: glob)
    {
        name += Tools::URL_SEP;
        WIPdata wipData{ name.substr(name.rfind('/') + 1) };
        guard.push_back(wipData.lg());
        wipData.read();
        wipData.setPsychEmail(d_eMail);
        wipData.write();
    }

    return guard;
}

#include "psych.ih"

bool Psych::validRegistrationData(uint64_t *nip, uint8_t *field)
try
{
    lg() <<  __FILE__  << endl;

    *nip = requireNumber("nip");
    requireOneOf("email", "@");
    requireContents("pwd");
    requireContents("name");
    requireContents("lastName");

    if 
    (
        string pwd = d_cgi->param1("pwd");
            pwd != d_cgi->param1("pwd2")            || 
            not pwdRequirements(pwd)                ||
            *nip == 0
    )
    {
        lg() <<  __FILE__ " invalid registration data" << endl;
        return false;
    }

    *field = parseField();
    lg() <<  __FILE__ " registration data OK" << endl;
    return true;
}
catch (bool)
{
    lg() <<  "validRegistrationData failed" << endl;
    return false;
}


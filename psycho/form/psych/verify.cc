#include "psych.ih"

// receives: e-mail, pwd, mode: noPwd or login

void Psych::verify()
{
    lg() <<  __FILE__ << endl;

    if (not get(emailCGI()))
        return;

    string mode = d_cgi->param1("mode");

    if (mode == "noPwd")
        confirmNewPwd();
    else if (mode == "login")
        login();                    // 1st time: send mail with ack. nr.
    else
        d_display.homePage();
}




#include "psych.ih"

void Psych::verifyAck()
{
    lg() <<  __FILE__ << endl;

    if (not get(emailCGI()))
        return;

    d_display.append("email");

    uint16_t ack = Tools::valueOr(d_cgi->param1("ack"), 0);
    if (ack == 0 || ack != d_ack)
    {
        d_display.append({ "mode", "pwd" });
        d_display.out("ackerror.h");
        return;
    }

    d_ack = 0;
    put(emailCGI());

    d_display.out(
            "actions.h",
            {
                messages()
            }
        );
}




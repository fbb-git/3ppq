#include "psychclient.ih"

void PsychClient::get(string const &data)
{
    istringstream in(data);

    string iv;
    Tools::readS(in, iv, 8);

    Tools::readN(in, &d_ID);
    Tools::readN(in, &d_active);

    ostringstream out;
    out << in.rdbuf();

    in.str(Tools::decrypt(iv, out.str()));

    Tools::readN(in, &d_gender, 1);
    Tools::readN(in, &d_zipPwd);
    getline(in, d_ident);
    getline(in, d_name);
    getline(in, d_lastName);
    getline(in, d_eMail);
}

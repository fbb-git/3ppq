#include "psychclient.ih"

string PsychClient::toString() const
{
    lg() << "psychClient toString: zip pwd " << d_zipPwd << endl;

    ostringstream out;

    Tools::writeN(out, &d_gender, 1);
    Tools::writeN(out, &d_zipPwd);
    out <<  d_ident     << '\n' <<
            d_name      << '\n' <<
            d_lastName  << '\n' <<
            d_eMail     << '\n';

    string iv = Tools::iv();
    string enc = Tools::encrypt(iv, out.str());

    out = ostringstream{};

    Tools::writeS(out, iv);
    Tools::writeN(out, &d_ID);
    Tools::writeN(out, &d_active);

    return out.str() + enc;
}

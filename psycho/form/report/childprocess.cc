#include "report.ih"

void Report::childProcess()
{
    prepareDaemon();

    string logReport{ g_options.log() + ".report" };

    int fd = open(logReport.c_str(), O_WRONLY);     // lock the logfile
    flock(fd, LOCK_EX);    

    ofstream log{ logReport, ios::in | ios::ate };  // open the report
                                                    // construction log file

    log << "ID: " << getpid() << ". " << 
                DateTime{ DateTime::LOCALTIME } << '\n';       // ID marker

    fsplot(log);
    latex(log);
    dvipdf(log);
    zip(log);
    sendMail(log);

    log << endl;                                // extra line + flush

    flock(fd, LOCK_UN);

    d_wipData.remove();

    for (char const *name: Glob{ g_options.tmpDir() + d_pidCid + ".*" } )
        unlink(name);

    throw Tools::ForkChild::DONE;    
}



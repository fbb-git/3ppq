   #include "report.ih"

void Report::dvipdf(ostream &log) const
{
    string dviFile{ d_pathPrefix      + "dvi" };
    string pdfFile{ g_options.reportsDir() + d_pidCid + ".pdf" };

    string command{ g_config.findKeyTail("dvipdf:") + ' ' + 
                            dviFile + ' ' + pdfFile };

    log << command << '\n';
    Tools::childProcess(command);
}



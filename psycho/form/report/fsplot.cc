#include "report.ih"

void Report::fsplot(ostream &log) const
{
    string gnuplotInput{ d_pathPrefix + "gp" };
    string gnuplotData{ d_pathPrefix  + "dat" };

    DollarText::replace(gnuplotInput, 
                            g_options.moldsDir() + "gnuplot", 
                            { 
                                d_gnuplotEps, 
                                gnuplotData 
                            } 
                        );

    string csvPath{ g_options.reportsDir() + d_pidCid + ".csv" };

    {
        ofstream csv;
        Exception::open(csv, csvPath);            // produced CSV file
        csv << d_wipData;
    }

    string command{ g_options.binDir() + "fsplot " +
                            csvPath       + ' ' +
                            gnuplotInput  + ' ' +
                            gnuplotData   + ' ' +
                            d_fScores };


    log << command << '\n';
    Tools::childProcess(command);
}




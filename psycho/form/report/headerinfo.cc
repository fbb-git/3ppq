#include "report.ih"

string Report::headerInfo() const
{
    ostringstream out;
    
    uint32_t from = d_wipData.startTime();
    time_t timeValue = from;
    tm tmValue{ *localtime(&timeValue) };

    out << 
R"_1_(    \makecell[l]{Naam cli\"ent:} 
\\
    \makecell[l]{Cli\"ent identificatie:} &
    \makecell[l]{)_1_" <<  d_wipData.clientIdent()   << R"_2_(}
\\
    \makecell[l]{Afnamedatum:} &
    \makecell[l]{)_2_" << put_time(&tmValue, "%e %b %Y") << R"_3_(}
\\
    \makecell[l]{Psycholoog:} &
    \makecell[l]{)_3_" << d_psychFullName << "}\n";

    return out.str();
}

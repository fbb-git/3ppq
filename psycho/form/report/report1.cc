#include "report.ih"

Report::Report(WIPdata &wipData, size_t pwd, string const &psychFullName) 
:
    d_wipData(wipData),
    d_pwd(pwd),
    d_pidCid(wipData.pidCid()),
    d_pathPrefix(g_options.tmpDir() + d_pidCid + '.'),
    d_gnuplotEps(d_pathPrefix   + "eps"),
    d_fScores(d_pathPrefix      + "fscores"),
    d_latexInput(d_pathPrefix   + "latex"),
    d_psychFullName(psychFullName)
{}






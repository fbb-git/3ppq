#include "report.ih"

void Report::sendMail(ostream &out) const
{
    out << g_mailer.sendmail(
                d_wipData.psychEmail(),
                d_wipData.psychEmail(),
                "Resultaten 3ppq voor client " + d_wipData.clientIdent() + 
                                                " zijn beschikbaar",
                DollarText::replaceStream(
                    g_options.mailDir() + "results", 
                    {
                        d_psychFullName,
                        d_wipData.clientIdent()           // $1
                    }
                ),
                false                       // no child process, but calls
                                            // Mailer::mailInserter
            ) << '\n';
}

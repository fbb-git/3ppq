#include "report.ih"

void Report::zip(ostream &log) const
{
    if (chdir(g_options.reportsDir().c_str()) != 0)
    {
        log << "INTERNAL: cannot cd to " << g_options.reportsDir() << endl;
        throw Tools::MessageEnum::INTERNAL;
    }

    string command{ g_config.findKeyTail("zip:") + 
                        " -P " + to_string(d_pwd) + " -m " +
                        d_pidCid + ".zip " +
                        d_pidCid + ".pdf " +
                        d_pidCid + ".csv" };

    log << command << '\n';
    Tools::childProcess(command);
}

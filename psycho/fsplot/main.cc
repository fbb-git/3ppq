#include "main.ih"

namespace
{
    Arg::LongOption longOptions[] =
    {
        Arg::LongOption{"help", 'h'},
        Arg::LongOption{"version", 'v'},
    };
    auto longEnd = longOptions + size(longOptions);

#include "../basedir.f" // abs. path to the website's base directory

} // anonymous

Options g_options{ g_base };
ConfigFile  g_config{ g_options.config(), ConfigFile::RemoveComment };

            // m_ identifiers are only used by main and global functions

ostream     m_log{ 0 };
bool        m_debug = false;
ofstream    m_devNull{ 0 };
size_t      g_logID = 0;                        // user ID or system ID (= 0)

int main(int argc, char **argv)
try
{
    Arg &arg = Arg::initialize("hv", longOptions, longEnd, argc, argv);
    arg.versionHelp(usage, version, 4);

//    prepareLog();

    Ratings ratings{ argv[1] };             // extract the data from the csvs

    Fscores fscores(ratings.data());        // compute the factor scores

    DataFiles dataFiles{ fscores.table() };
    dataFiles.gnuplot(argv[3]);

    dataFiles.scoresTable(argv[4]);

    Gnuplot gnuplot{ ratings, fscores.scores() };

    int ret = gnuplot.plot(argv[2]);

//    flushLogs(s_log);

    return ret;
}
catch (int x)
{
    return Arg::instance().option("hv") != 0 ? 0 : x;
}
catch (exception const &exc)
{
    cout << exc.what() << endl;
    return 1;
}



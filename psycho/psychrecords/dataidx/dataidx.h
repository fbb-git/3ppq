#ifndef INCLUDED_DATAIDX_
#define INCLUDED_DATAIDX_

#include <iosfwd>
#include <fstream>

class DataIdx
{
    enum Header                 // indices in d_header
    {
        N_KEYS,                 // keys in use
        N_BITS,                 // # bits used for the prime computations
        SIZE,                   // locations available
        sizeHeader
    };

    static constexpr uint16_t   s_primeBits = 6;        // inital # prime bits
    static constexpr double     s_loadFactor = 0.6;

    std::string d_path;
    std::ifstream d_stream;

    uint16_t d_header[sizeHeader];

    public:
        struct IdxEntry
        {
            std::string key;        // Tools::KEY_SIZE bytes
            uint64_t dataOffset;
        };

        struct Info
        {
            std::string key;        // Must be same fields as IdxEntry
            uint64_t dataOffset;

            uint64_t idxOffset;
        };

        DataIdx() = default;        // not used by form but indirectly by,
                                    // e.g., psychrm, to create a plain Psych
                                    // object.

        DataIdx(std::string dataIdxPath, uint16_t nPrimeBits = s_primeBits);

        Info read(uint64_t offset); // read the entry at a given offset

        size_t size() const;

    private:
        static uint64_t idx2offset(size_t idx);

        size_t keyHash(std::string const &key) const;

        Info get(size_t idx);
};

inline size_t DataIdx::size() const
{
    return d_header[N_KEYS];
}

#endif

#include "dataidx.ih"

DataIdx::DataIdx(string dataIdxPath, uint16_t primeBits)
:
    d_path(move(dataIdxPath))
{
    Exception::open(d_stream, d_path);

       // get the header
    Tools::readB(d_stream, d_header, sizeof(d_header));
}


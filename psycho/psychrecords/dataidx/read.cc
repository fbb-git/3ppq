#include "dataidx.ih"

DataIdx::Info DataIdx::read(uint64_t offset)
{
    d_stream.seekg(offset);

    Info ret;

    Tools::readS(d_stream, ret.key,         // read IdxEntry data
                             Tools::KEY_SIZE);   
    Tools::readB(d_stream, &ret.dataOffset);

    ret.idxOffset = offset;                 // store the dataIdx offset

    return ret;
}


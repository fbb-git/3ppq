#ifndef INCLUDED_DATASTORE1_
#define INCLUDED_DATASTORE1_

#include <string>
#include <fstream>

#include "../dataidx/dataidx.h"

// keys must be Tools::KEY_SIZE bytes. NOT checked!

class LockGuard;

class DataStore1
{
    struct Header
    {
        uint64_t fileSize;
        uint16_t orderNr;
        uint16_t reserved1[3];
    };

    Header d_header;

    std::string d_path;
    std::ifstream d_data;
    DataIdx d_dataIdx;

    public:
        struct BlockHeader
        {
            uint64_t nextOffset;
            uint64_t idxOffset;
            uint32_t dataSize;
        };

        DataStore1(std::string const &path);     // full path to the data file

                                        // at 0: retrieve the 1st block
        BlockHeader getBlock(std::string *data, uint64_t offset);

        static uint64_t firstOffset();
        uint64_t fileSize() const;
        size_t size() const;            // N entries in the index

    private:
                                        // merely retrieve and write
                                        // BlockHeaders. 
        BlockHeader getBlockHeader(uint64_t offset);

        static size_t dataSpace(uint64_t blockOffset, BlockHeader const &bh);
};

// A common setup of data storage/updates and retrieval works like this:
//
//  Storage:
//  1. determine a key
//  2. obtain information: part I: not encrypted, part II: encrypted
//  3. encrypt part II: write to a string
//  4. determine the size of the unencrypted data (this may conveniently be
//      stored in the unencrypted data, or it may be written as the first
//      value in the stored data
//  4. append the encrypted bytes to the string holding the unencrypted bytes
//      (+ their size). 
//  5. store (or update) the data for the specified key.
//
//  Retrieval:
//  1. determine the key.
//  2. retrieve the key's data
//  4. determine the location of the encrypted data
//  5. decrypt the encrypted data
//  6. convert the unencrypted bytes to, e.g., the unencrypted data struct
//  7. convert the decrypted bytes to, e.g., the confidential data struct

// static
inline uint64_t DataStore1::firstOffset()
{
    return sizeof(Header);
}

inline uint64_t DataStore1::fileSize() const
{
    return d_header.fileSize;
}

inline size_t DataStore1::size() const
{
    return d_dataIdx.size();
}
    
#endif


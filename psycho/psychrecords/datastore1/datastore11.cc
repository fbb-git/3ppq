#include "datastore1.ih"

DataStore1::DataStore1(string const &path)
:
    d_path(path),
    d_dataIdx(path + ".idx")
{
    Exception::open(d_data, path);

    Tools::readB(d_data, &d_header, sizeof(Header)); 
}

#include "datastore1.ih"

DataStore1::BlockHeader DataStore1::getBlock(string *data, uint64_t offset)
{
    BlockHeader bh{ getBlockHeader(offset) };
    Tools::readS(d_data, *data, bh.dataSize);
    return bh;
}

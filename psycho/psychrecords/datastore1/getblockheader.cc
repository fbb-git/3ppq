#include "datastore1.ih"

DataStore1::BlockHeader DataStore1::getBlockHeader(uint64_t blockOffset)
{
    d_data.seekg(blockOffset);          // go to the data block's location

    BlockHeader ret;
                                        // and write the block header
    Tools::readB(d_data, &ret, sizeof(BlockHeader));  

    return ret;
}

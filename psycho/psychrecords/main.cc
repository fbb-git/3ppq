#include "main.ih"

namespace {

Arg::LongOption longOptions[] =
{
    Arg::LongOption{"convert", 'c'},        // convert to 1 file / psych.
    Arg::LongOption{"expired", 'e'},        // number of days
    Arg::LongOption{"list", 'l'},
    Arg::LongOption{"log", 'L'},
    Arg::LongOption{"modify", 'm'},
    Arg::LongOption{"remove", 'r'},

    Arg::LongOption{"help", 'h'},
    Arg::LongOption{"version", 'v'},
};
auto longEnd = longOptions + size(longOptions);

}   // anonymous

ostream     m_log{ 0 };
bool        m_debug = false;
ofstream    m_devNull{ 0 };
size_t      g_logID = 0;                        // user ID or system ID (= 0)

int main(int argc, char **argv)
try
{
    Arg &arg = Arg::initialize("ce:hlL:mr:v", longOptions, longEnd, 
                               argc, argv);

    arg.versionHelp(usage, version, 1);

    m_log.setstate(ios::badbit);

    Records records;
    records.process();
}
catch (int)
{
    return 0;
}
catch (exception const &exc)
{
    if (Arg::instance().option("hv"))
        return 0;

    cout << exc.what() << '\n';
    return 1;
}
catch (...)
{
    cout << "unexpected exception\n";
    return 1;
}

#include "records.ih"

void Records::convert()
{
    cout << "Defined entries: " << d_data.size() << '\n';

    DataStore1::BlockHeader bh{ DataStore1::firstOffset() };

    Psych psych;

    for (size_t idx = 0; idx != d_data.size(); ++idx)
    {
        string data;

        uint64_t offset = bh.nextOffset;

        bh = d_data.getBlock(&data, offset);

        psych.read(data);

        cout << "Entry at data.idx offset " << bh.idxOffset << " (0x" <<
                hex << bh.idxOffset << "),\n"
                "data offset " << offset << 
                " (0x" << hex << offset << dec << ')' << '\n' <<
                "next data offset " << bh.nextOffset << 
                " (0x" << hex << bh.nextOffset << dec << ')' << '\n' <<
                psych << 
                "psychologist's data in " << 
                                    Tools::b64hash(psych.eMail()) << '\n' <<
                endl;

        psych.put();
    }
}

#ifndef INCLUDED_RECORDS_
#define INCLUDED_RECORDS_

#include <string>

#include "../../support/tools/tools.h"
#include "../datastore1/datastore1.h"

namespace FBB
{
    class Arg;
}

class Records
{
    FBB::Arg &d_arg;
    DataStore1 d_data;

    enum { SHIFT = Tools::KEY_SIZE + sizeof(uint64_t) } ;

    public:
        Records();
        void process();

    private:
        void convert();                     // from 0.98.00 to 1.01.00

        void list();
        void expired() const;       // ORG
        void modify() const;        // ORG
        void remove() const;        // ORG

        void removeExistingNew() const;                 // ORG
        static void remove(std::string const &path);    // ORG
};
        
#endif

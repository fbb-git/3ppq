#include "main.ih"

int main(int argc, char **argv)
try
{
    if (argc == 1)
        throw Exception{} << 
                    "provide argument e to encrypt, d to decrypt cin";

    string action{ argv[1] == "e"s ? "--encrypt " : "--decrypt " };

    Process process{
        Process::CIN | Process::COUT,
        Process::NO_PATH,
        "/usr/bin/gpg "
        "--homedir /home/frank/src/3ppq/src/pgphandler/.gnupg "
        "--batch "
        "--pinentry-mode loopback "
        "--passphrase-fd 0 " 
        "--decrypt thefile.gpg"
    };

    string pwd = password();

    cout << "password = `" << pwd << "'\n";

    process.start();

    process << pwd << eoi;

    cerr << process.exitStatus() << '\n';

    cout << "here's the error output:\n" << 
                                    process.childErrStream().rdbuf() << '\n';

    cout << "here's the output:\n" << process.childOutStream().rdbuf() << '\n';

//    cout << pwd << '\n';

//    process.start();
//    cout << process.childOutStream().rdbuf();

}
catch (exception const &exc)
{
    cerr << exc.what() << '\n';
}


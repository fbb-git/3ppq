#include "datastore.ih"

    // {webbase} is defined by g_base:
    // nextnr is in {webbase}/data, not in {webbase}/data/psychdir
    // the relative location is used to simplify its initialization, as
    // the cwd, when called, is data/psychdir

char const DataStore::s_nextNr[] = "../nextnr";


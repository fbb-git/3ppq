#ifndef INCLUDED_DATASTORE_
#define INCLUDED_DATASTORE_

#include <string>

// keys must be Tools::KEY_SIZE bytes. NOT checked!

class DataStore
{
    std::string d_data;                 // path to {webbase}/data/psych 

    static char const s_nextNr[];       // nextnr file, in {webbase}/data/

    public:
        DataStore() = default;          // not used by form, but by, e.g.,
                                        // psychrm to create a plain Psych
                                        // object.
                                        
        DataStore(std::string const &path);     // full path to the 
                                                // psych data directory

        // used to be: get
        bool read(std::string *data, std::string const &key);

        // used to be: add, update
        void write(std::string const &key, std::string const &data);

        void rename(std::string const &oldName, std::string const &newName);

        bool erase(std::string const &key);     // erase d_data/key

        uint16_t nextNr();              // returns the next nr at each call

        bool exists(std::string const &key);    // true if already exists

// maybe not needed?
//        LockGuard lg();                 // after calling lg() d_data and
                                        // d_dataIdx are open, remaining open 
                                        // until LockGuard's destructor is
                                        // called. 

    private:
        void ensure(char const *name);  // ensure the existence of
                                        // the file d_data/name

};

#endif


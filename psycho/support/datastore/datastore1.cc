#include "datastore.ih"

DataStore::DataStore(string const &path)// data/psychdir/<hashed filename
:                                       // of the psychologist, e.g.
    d_data(path)                        // data/psychdir/ZnJhbmtAbG9jYWxob3N0
{
    if (d_data.back() != '/')               // append final '/'
        d_data += '/';

        // the ./data directory must exist (created by ./install

//    ensure("count");                    

    ensure(s_nextNr);                       // if not existing, create 
}

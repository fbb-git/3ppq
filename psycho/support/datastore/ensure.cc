#include "datastore.ih"

void DataStore::ensure(char const *name)
{
    string filename = d_data + name;

    if (not Tools::rwExists(filename))
    {
        ofstream out{ filename };
        out << "0\n";
    };
}

#include "datastore.ih"

bool DataStore::erase(string const &key)
{
    ::lg() << __FILE__ << ": erase " << key << endl;

    return unlink((d_data + key).c_str()) == 0;
}


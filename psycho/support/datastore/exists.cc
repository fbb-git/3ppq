#include "datastore.ih"

bool DataStore::exists(string const &key)
{
    return Tools::rwExists(d_data + key);
}

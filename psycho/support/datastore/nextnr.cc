#include "datastore.ih"

uint16_t DataStore::nextNr()
{
    string filename{ d_data + s_nextNr };

    LockGuard lg{ filename };               // lock 'g_base' data/nextnr

    fstream io;
    Exception::open(io, filename, ios::in | ios::out);

    uint16_t nr;
    io >> nr;
    ++nr;

    io.seekp(0);
    io << nr << '\n';

    ::lg() << __FILE__" next nr = " << nr << endl;

    return nr;
}

#include "datastore.ih"

bool DataStore::read(string *data, string const &key)
try
{
    ::lg() << __FILE__ << ": read " << key << endl;

    string path{ d_data + key };

    Stat stat{ path };

    if (not stat)
        return false;

    ifstream in;
    Exception::open(in, path);

    Tools::readS(in, *data, stat.size());

    return true;
}
catch (...)
{
    return false;
}


#include "datastore.ih"

void DataStore::rename(string const &oldName, string const &newName)
{
    Tools::rename(d_data + oldName, d_data + newName);
}

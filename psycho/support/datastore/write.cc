#include "datastore.ih"

    // write a file in the psych directory named 'key'

void DataStore::write(std::string const &key, string const &data)
{
    ::lg() << __FILE__": write " << key << endl;

    ofstream psych;
    Exception::open(psych, d_data + key);   // create/rewrite the psych file

    Tools::writeS(psych, data);             // write the data
}



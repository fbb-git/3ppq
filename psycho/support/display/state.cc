#include "display.ih"

// static
void Display::state(string const &path)
{
    lg() << "state: " << path.substr(path.rfind('/') + 1) << endl;
}

//#define XERR
#include "endecrypt.ih"

std::string EnDeCrypt::process(std::string const &in)
{
    string ret;

    for (size_t inIdx = 0, inEnd = in.length(), keyIdx = 0; 
            inIdx != inEnd;
                ++inIdx, ++keyIdx)
    {
        if (keyIdx == d_key.size())
            keyIdx = 0;

        ret.push_back(in[inIdx] xor d_key[keyIdx]);
    }

    return ret;
}

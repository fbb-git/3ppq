#include "global.ih"

void flushLogs(ostringstream &msg)
{
    int fd = open(g_options.log().c_str(), O_WRONLY);   // lock the logfile
    flock(fd, LOCK_EX);    

    ofstream log;                           // open the collective log file
    Exception::open(log, g_options.log(), ios::in | ios::ate);
    log << msg.str() << '\n';               // append this process's logs
}

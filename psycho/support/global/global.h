#ifndef INCLUDED_GLOBAL_H_
#define INCLUDED_GLOBAL_H_

#include <iosfwd>

#include <bobcat/log>
#include <bobcat/level>
#include <bobcat/configfile>

enum LogType
{
    STD,
    DEBUG
};
    
extern size_t           g_logID;    // user ID or system ID (= 0)

extern FBB::ConfigFile  g_config;

void prepareLog();
void flushLogs(std::ostringstream &msg);    // flush this process's logs to 
                                            // the std log file 
std::ostream &lg(LogType type = STD);       // use psychID 0 for system 
                                            // related

#endif

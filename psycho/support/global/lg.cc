#include "global.ih"

ostream &lg(LogType type)
{
    return type == STD and not m_debug ?
                m_devNull
            :
                m_log << g_logID << ": ";
}

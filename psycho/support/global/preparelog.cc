#include "../global/global.ih"

void prepareLog()
{
    string log = g_config.findKeyTail("log:");  // name of the global logfile

    if (log.empty() || log == "off")            // no name specified, or 'off'
    {
        m_log.setstate(ios::failbit);           // insertions are ignored
        return;
    }

    m_debug =  log == "debug";

    m_devNull.setstate(ios::badbit);            // insertions are ignored
}

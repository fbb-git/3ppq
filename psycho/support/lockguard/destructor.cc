#include "lockguard.ih"

LockGuard::~LockGuard()
{
    if (s_locked[d_path] != 0)
        --s_locked[d_path];

    if (s_locked[d_path] == 0)
    {
        lg() << __FILE__" unlocked " << d_path << endl;
        flock(d_fd, LOCK_UN);               // else unlock, and rm from the
                                            // map of locked streams
        s_locked.erase(d_path);             
    }
}

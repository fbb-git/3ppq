#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/file.h>

#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    fstream fs("demo", ios::in | ios::out);

    int fd = ::open("demo", 0);
    if (fd == -1)
    {
        cout << "failed\n";
        return 1;
    }

    flock(fd, LOCK_EX);

    fs.seekp(0, ios::end);
    fs << "hello world\n";
    
    string line;
    cout << "? ";
    getline(cin, line);
}

#ifndef INCLUDED_LOCKGUARD_
#define INCLUDED_LOCKGUARD_

#include <iosfwd>
#include <string>
#include <unordered_map>

class LockGuard
{
    std::string d_path;             // file locked by this lockquard
    int d_fd;                       // fd associated with d_data

    static std::unordered_map<std::string, size_t> s_locked;

    public:
        LockGuard(std::string const &path);
        ~LockGuard();
};
        

#endif

#include "lockguard.ih"

LockGuard::LockGuard(string const &path)
:
    d_path(path),
    d_fd(::open(path.c_str(), 0))
{
    if (d_fd == -1)
    {
        lg() << "can't ::open " << path << endl;
        throw Tools::NO_DATA;
    }

    if (s_locked.find(path) == s_locked.end())              // not yet locked?
    {
        if (flock(d_fd, LOCK_EX) == 0)                      // then lock
            lg() << __FILE__" locked " << path << endl;
        else
        {
            lg() << "can't lock " << path << endl;
            throw Tools::NO_DATA;
        }
    }
    ++s_locked[path];
}














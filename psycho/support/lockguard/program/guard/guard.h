#ifndef INCLUDED_GUARD_
#define INCLUDED_GUARD_

#include <iosfwd>
#include <unordered_map>

class Guard
{
    static std::unordered_map<std::string, size_t> s_locked;

    public:
        Guard(std::fstream &out);

    private:
};
        
#endif

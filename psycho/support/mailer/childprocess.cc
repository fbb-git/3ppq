#include "mailer.ih"

void Mailer::childProcess()
{
    prepareDaemon();

    mailInserter();

    throw Tools::ForkChild::DONE;
}

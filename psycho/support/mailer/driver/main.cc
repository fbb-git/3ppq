#include <iostream>
#include <string>
#include <fstream>

// #include <thread>

#include <bobcat/log>
#include <bobcat/fork>
#include <bobcat/configfile>

#include "../mailer.h"

using namespace std;
using namespace FBB;

ConfigFile g_config;

ofstream logfile2{ "logfile2", ios::in | ios::ate };    // must exist !

Log m_log{ "logfile" };
int         m_logFD = -1;
size_t      g_logID = 0;

class MailFork: public Fork
{
    private:
        void childProcess() override;
        void parentProcess() override;
};

void MailFork::childProcess()
{
    prepareDaemon();

//    this_thread::sleep_for(chrono::seconds(5));

    Mailer mailer;

    logfile2 << 
    mailer.sendmail("frank@localhost", "frank@localhost", "support driver",
        "\n"
        "This is mail from support/mailer/driver\n"
    ) << endl;

}

void MailFork::parentProcess()
{}
    
int main()
try
{
//    log2.open(

    MailFork mailFork;
    mailFork.fork();

}
catch (...)
{
    cerr << "caught exception\n";
    return 1;
}


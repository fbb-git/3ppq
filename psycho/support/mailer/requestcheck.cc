#include "mailer.ih"

bool Mailer:: requestCheck(ostringstream &log, string const &mailRequest, 
                            string const &to, string const &subject, 
                            string const &txt)

{
    if (mailRequest == "log")
    {
        log << "would have sent mail to `" << to << "', subject: `" << 
                    subject << "', contents: `" << txt << '\'';
        return true;
    }

    if (mailRequest == "off")
    {
        log << " mail to " << to << " not sent: mailRequest = " << 
                        mailRequest;
        return true;
    }

    return false;
}

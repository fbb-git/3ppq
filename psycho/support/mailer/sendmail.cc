//#include <string>
#include "mailer.ih"

std::string Mailer::sendmail(string const &psychEmail, string to, 
                        string const &subject,    string const &txt,
                        bool viaChild)
{
    ostringstream log;
    string mailRequest = g_config.findKeyTail("mail:");

    if (requestCheck(log, mailRequest, to, subject, txt))
        return log.str();

//    if (mailRequest == "log")
//    {
//        log << "would have sent mail to `" << to << "', subject: `" << 
//                    subject << "', contents: `" << txt << '\'';
//        return log.str(); 
//    }
//
//    if (mailRequest == "off")
//    {
//        log << " mail to " << to << " not sent: mailRequest = " << 
//                        mailRequest;
//        return log.str();
//    }

        // using '@' results in compilation warning when precompiled headers
        // are used. Calling requestCheck containing the above commented-out
        // statements or using "@" instead of '@' in the next statement
        // does not produce the warning. 
    if (mailRequest.find('@') != string::npos)
    {
        log << "mail to " << to << " is sent to " << mailRequest << '\n';
        to = mailRequest;
    }
    else if (mailRequest == "psych")
    {
        to = psychEmail;
        log << "mail to " << to << " is sent to the psych: " << to << '\n';
    }

    d_txt = txt;
    d_to = to;
    d_subject = subject;

    if (viaChild)
        fork();
    else
        mailInserter();

    log << "mail (" << d_subject << ") sent to " << d_to << '\n';

    return log.str();
}







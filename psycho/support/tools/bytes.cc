#include "tools.ih"

void Tools::bytes(ostream &out, string const &str)
{
    for (char ch: str)
        out << static_cast<int>(ch) << ' ';
}

#include "tools.ih"

void Tools::errorState(string const &path)
{
    lg() << '`' << path << "': error state" << endl;
    throw INTERNAL;
}

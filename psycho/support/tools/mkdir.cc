#include "tools.ih"

// static
void Tools::mkdir(string const &path)
{
    if (::mkdir(path.c_str(), 0770) != 0 or chmod(path.c_str(), 0770) != 0)
    {
        lg() << "mkdir " << path << ": " << errnodescr << endl;
        throw CHILD;
    }
}

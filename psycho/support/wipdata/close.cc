#include "wipdata.ih"

void WIPdata::close()
{
    if (not d_io.good())
        Tools::errorState(path());

    d_io.close();
}

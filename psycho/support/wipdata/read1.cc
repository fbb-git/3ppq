#include "wipdata.ih"

void WIPdata::read(istream &in)
{
    ::lg() << "WIPdata::read" << endl;

    Tools::readN(in, &d_psychID);

    getline(in, d_clientIdent);

    Tools::readN(in, &d_start);
    Tools::readN(in, &d_clientLogin);

    for (auto &login: d_otherLogin)
        Tools::readN(in, &login);

    Tools::readS(in, d_selfRatings, Tools::N_QUESTIONS);
    Tools::readS(in, d_metaRatings, Tools::N_QUESTIONS);

    for (auto &ratings: d_otherRatings)
        Tools::readS(in, ratings, Tools::N_QUESTIONS);

    string iv;
    Tools::readS(in, iv, Tools::IV_SIZE);

    uint16_t size;
    Tools::readN(in, &size);

    string encrypted;
    Tools::readS(in, encrypted, size);

    string decrypted;
    istringstream addresses{ decrypted = Tools::decrypt(iv, encrypted) };

    Tools::readN(addresses, &d_gender);
    getline(addresses, d_clientName);
    getline(addresses, d_clientLastName);
    getline(addresses, d_psychEmail);

    for (auto &mail: d_otherEmail)
        getline(addresses, mail);
}





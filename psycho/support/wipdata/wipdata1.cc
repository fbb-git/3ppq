#include "wipdata.ih"

WIPdata::WIPdata(std::string const &psychEmail, 
                      uint16_t psychID, string const &clientIdent, 
                      string const &clientName, 
                      string const &clientLastName, bool gender)
:
    d_path(path(psychID, clientIdent)),
    d_psychID(psychID),
    d_clientIdent(clientIdent),
    d_gender(gender),
    d_clientName(clientName),
    d_clientLastName(clientLastName),
    d_start(time(0)),
    d_clientLogin(Tools::random(1000, 9999)),
    d_selfRatings(Tools::N_QUESTIONS, 0),
    d_metaRatings(Tools::N_QUESTIONS, 0),
    d_psychEmail(psychEmail)
{
            // Create the wip file. Only the psychologist can create this
            // WIP file. In theory two `Psych::inviteClient' calls might
            // arrive at the same time, but in practice that's not possible.
            // Even if, somehow, two activations would be received shortly
            // after each other then that wouldn't matter, since the 1st one
            // would have created the WIP file and the 2nd one would encounter
            // a BUSY exception.

    if (not Tools::rwExists(d_path))
        Exception::open(d_io, d_path, ios::in | ios::out | ios::trunc);
    else
    {
        ::lg() << d_path << " already exists" << endl;
        throw Tools::BUSY;
    }

    for (auto &login: d_otherLogin)
        login = Tools::random(1000, 9999);

    d_selfRatings.resize(Tools::N_QUESTIONS);
    d_metaRatings.resize(Tools::N_QUESTIONS);

    for (auto &ratings: d_otherRatings)
        ratings.resize(Tools::N_QUESTIONS);

    write();
    d_io.close();
}








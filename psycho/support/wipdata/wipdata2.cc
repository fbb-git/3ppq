#include "wipdata.ih"

WIPdata::WIPdata(string const &query)
:
    d_path(
            g_options.dataDir() + 
            query.substr(0, query.find_first_of(Tools::URL_SEP))
          )
{
    size_t dot = query.find('.');
    size_t equal = query.find_first_of(Tools::URL_SEP, dot);

    if (equal == string::npos)          // invalid query
    {
        ::lg() << "invalid query `" << query << '\'' << endl;
        throw Tools::QUERY;
    }

    d_psychID = stoul(query.substr(0, dot));
    ++dot;
    d_clientIdent = query.substr(dot, equal - dot);
}

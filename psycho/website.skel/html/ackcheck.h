<body class="full"> 
    <img src="/images/3ppqUL.jpg" class=logo>
<form>
<div class="container">

<h1>Uw registratie is verwerkt</h1>

Ter controle van uw e-mail-adres ontvangt u, nadat u voor de eerste keer
hebt ingelogd, een e-mail waarin een verificatienummer is vermeld.
<p>

Nadat u voor de eerste keer hebt ingelogd verschijnt er eenmalig een
web-pagina waarin u wordt gevraagd het per e-mail toegestuurd
verificatienummer te vermelden.

<p>
Het is mogelijk dat die e-mail in uw spam-box belandt. Controleer daarom
eveneens uw spam-box wanneer de mail niet binnen enige minuten is ontvangen.

<p>

Klik op de knop <em>Inloggen</em> om in te loggen.
<p>
Door op de knop <em>3ppq homepage</em> te klikken keert u terug naar de
3ppq-homepage. 
<p>

<div class=lightgrey>
<table>
<tr>
<td>
    <input type="submit" value="Inloggen" 
        onclick="window.open('/inloggen.html', '_self'); return false;">
</td>
<td>
    <input type="submit" value="3ppq homepage" 
        onclick="window.open('/index.shtml', '_self'); return false;">
</td>
</table>
</div>

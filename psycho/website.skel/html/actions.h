    <script src="/js/preamble.js"></script>
    <script src="/js/actions.js"></script>
</head>

<body class="full"> 
    <img src="/images/3ppqUL.jpg" class=logo>
<form name="form" action="/bin/form" method="post">

<nav class="top-menu-markup">                                               
    <ul class="spread">
        <li class="li-linewise">
            <a href="/home.shtml" title="3ppq home-page (en uitloggen)" >
                <img border=0 src="/images/home.png" height=16em >
            </a>
        </li>
        <li class="li-linewise">
            <a href="#" title="Activiteiten m.b.t. cli&euml;nten" 
               onclick="validate('clientPage')" 
            >
                Cli&euml;nten 
            </a>
        </li>
        <li class="li-linewise">
            <a href="/documents.shtml" title="Download documenten"
            >
                Documenten
            </a>
        </li>
        <li class="li-linewise">
            <a href="#" title="Wijzig uw eigen gegevens"
                onclick="validate('profilePage')"
            >
                Profiel
            </a>
        </li>
        <li class="li-linewise">
            <a href="/psychcontact.shtml" title="3ppq contact informatie"
            >
                Contact
            </a>
        </li>
    </ul>                                                                   
</nav>                                                                      

<div class="container">
 
<p>


Door op &#x2018;cli&euml;nten&#x2019; te klikken kunt u een afname van De 3ppq
starten. Voorafgaand daaraan dient een <a href=/voorbereidingsgesprek.html
target=_blank>voorbereidingsgesprek</a> met de cli&euml;nt te hebben
plaatsgevonden.  <p>

Onder &#x2018;documenten&#x2019; kunt u achtergrondinformatie vinden, waaronder
een uitgebreid gebruiksprotocol, de drie versies van de vragenlijst en online
beschikbare achtergrondartikelen.
<p>

Onder &#x2018;profiel&#x2019; kunt u uw eigen gegevens aanpassen. 
<p>

Voor vragen en opmerkingen vindt u de adressen onder &#x2018;contact&#x2019;.
<p>

N.B. Alvorens 3ppq.nl in gebruik te nemen, is het raadzaam zelf een <a
href=/proefafname.html target=_blank>proefafname</a> uit te voeren met uzelf
als cli&euml;nt en als anderen.  <p>

<!-- messages for the psychologist -->
$0

